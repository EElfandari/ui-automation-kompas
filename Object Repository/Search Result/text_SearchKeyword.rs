<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>text_SearchKeyword</name>
   <tag></tag>
   <elementGuidId>f7da661e-0469-42ec-9f51-f3746378d87f</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Gabung Kompas.com+'])[2]/following::div[4]</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//div[@class = 'title__content']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.title__content</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>fa676694-b395-4fbd-8f5e-3611fdd069a3</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>title__content</value>
      <webElementGuid>a78a0346-81b6-4648-9f49-7100e8095e7a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Pencarian &quot;ANTAM&quot;</value>
      <webElementGuid>7ecbfd3b-02f9-4e0b-9fa6-b19ed9b8d6e8</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[@class=&quot;js csstransitions&quot;]/body[@class=&quot;theme--search&quot;]/div[@class=&quot;wrap&quot;]/div[@class=&quot;container clearfix&quot;]/div[@class=&quot;row mt2 col-offset-fluid clearfix&quot;]/div[@class=&quot;col-bs10-7&quot;]/div[@class=&quot;title clearfix&quot;]/div[@class=&quot;title__content&quot;]</value>
      <webElementGuid>f736986c-efa8-46a3-aad6-6f5ed8187c90</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Gabung Kompas.com+'])[2]/following::div[4]</value>
      <webElementGuid>b3e93675-8a42-4cae-8c5b-0c77888ffb58</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='KOMPAS.com Search Engine'])[1]/preceding::div[5]</value>
      <webElementGuid>4bc83263-5350-4cb0-9d02-ab6768a95ebc</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='About 113,000 results (0.18 seconds)'])[1]/preceding::div[11]</value>
      <webElementGuid>0f85e095-3725-422a-9f7d-b877b0219499</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Pencarian &quot;ANTAM&quot;']/parent::*</value>
      <webElementGuid>4d01303d-bbe8-44f5-8dd1-4d3acfdf50f3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[3]/div/div/div</value>
      <webElementGuid>2d2dbf9d-3a67-4010-a026-f75de4c50962</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = 'Pencarian &quot;ANTAM&quot;' or . = 'Pencarian &quot;ANTAM&quot;')]</value>
      <webElementGuid>0c000c73-f03b-4343-89b3-9b04a2cb2af6</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
