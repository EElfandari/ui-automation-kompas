<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_CloseGoogleAcc</name>
   <tag></tag>
   <elementGuidId>2df6db4e-3f57-4591-a2ca-f16651af5f77</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@class = 'Bz112c Bz112c-r9oPif']</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Kompas.com'])[1]/preceding::*[name()='svg'][1]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>svg.Bz112c.Bz112c-r9oPif</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>svg</value>
      <webElementGuid>122ef137-733b-4771-9dea-6c57733355d6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>Bz112c Bz112c-r9oPif</value>
      <webElementGuid>f7e91106-ccf4-4461-b7be-05c9ae545ce2</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xmlns</name>
      <type>Main</type>
      <value>https://www.w3.org/2000/svg</value>
      <webElementGuid>94fb9085-aa0e-4adc-b5cb-18242b039f15</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>viewBox</name>
      <type>Main</type>
      <value>0 0 24 24</value>
      <webElementGuid>930c05f3-b91e-4816-a1e2-daada007905a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>fill</name>
      <type>Main</type>
      <value>#5f6368</value>
      <webElementGuid>1ce971f9-e7a8-4255-8ca0-566ccf48aa78</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;close&quot;)/svg[@class=&quot;Bz112c Bz112c-r9oPif&quot;]</value>
      <webElementGuid>d5a4ff83-1a32-4836-b913-3a739786247c</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Kompas.com'])[1]/preceding::*[name()='svg'][1]</value>
      <webElementGuid>2df64b24-fc58-437d-a6d3-89be71dc26b0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Login dengan Google'])[1]/preceding::*[name()='svg'][3]</value>
      <webElementGuid>4f8265c1-0523-409b-b913-28b4006a5a28</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
