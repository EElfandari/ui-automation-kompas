<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>icon_Login</name>
   <tag></tag>
   <elementGuidId>969f0d1e-0a92-43ce-9a05-6f4a64884215</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//i[@id='sso__icon__login_top']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#sso__icon__login_top</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>i</value>
      <webElementGuid>edb42637-0557-4253-8d43-703bf3fcb174</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>sso__icon__login sso__avatar</value>
      <webElementGuid>73e7b7e6-cb2f-4055-b388-22bbc691b134</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>sso__icon__login_top</value>
      <webElementGuid>ac7b8d7b-b904-4d1a-8273-891fbb44cb8c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;sso__icon__login_top&quot;)</value>
      <webElementGuid>343bf559-505c-4a02-914c-72295af0d0e8</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-avatar</name>
      <type>Main</type>
      <value>bg</value>
      <webElementGuid>e59814af-1c5c-41aa-940e-28264aa564cc</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-init</name>
      <type>Main</type>
      <value>RT</value>
      <webElementGuid>36613333-c2d4-4e0e-9b6c-c93f9e494794</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//i[@id='sso__icon__login_top']</value>
      <webElementGuid>6c6e48da-df2d-4516-876b-92d64ee20474</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//li/a/i</value>
      <webElementGuid>b2deb678-076a-4fe4-bc5f-030d617343e1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//i[@id = 'sso__icon__login_top']</value>
      <webElementGuid>5f89987c-c18f-4f69-8ee8-392894454229</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
